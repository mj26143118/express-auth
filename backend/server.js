//server.js
const express = require('express');
const session = require('express-session');
const User = require('./models/User');
const bcrypt = require('bcryptjs');
const cors = require('cors');


require('dotenv').config();
require('./db'); 

const app = express();
app.use(express.json());
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
}));

// for allow from  React frontend
app.use(cors({
    origin: 'http://localhost:5000', // Allow this origin to make cross-origin requests
    credentials: true // Allow credentials (cookies, authorization headers, etc.)
}));

// Register route
app.post('/register', async (req, res) => {
  const { username, email, password } = req.body;
  try {
    const user = new User({ username, email, password });
    await user.save();
    res.status(201).send('User created');
  } catch (error) {
    if (error.code === 11000) { // MongoDB duplicate key error
      return res.status(409).send('Email already exists');
    }
    res.status(500).send('Internal Server Error');
  }
});

// Login route
app.post('/login', async (req, res) => {
    const { email, password } = req.body;
    try {
        const user = await User.findOne({ email });
        if (!user || !(await bcrypt.compare(password, user.password))) {
            return res.status(401).send('Authentication failed');
        }
        req.session.userId = user._id;
        res.send('Logged in successfully');
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Logout route
app.post('/logout', (req, res) => {
    req.session.destroy();
    res.send('Logged out successfully');
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

import React from 'react';
import Register from './components/Register';  // Adjusted path
import Login from './components/Login';        // Adjusted path
import Logout from './components/Logout';      // Adjusted path

function App() {
  return (
    <div className="App">
      <h1>Express Auth MJ</h1>
      <Register />
      <Login />
      <Logout />
    </div>
  );
}

export default App;

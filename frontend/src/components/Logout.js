//frontend\src\components\Logout.js

import React from 'react';
import axios from 'axios';

function Logout() {
  const handleLogout = async () => {
    try {
      await axios.post('http://localhost:3000/logout');
      alert('Logged out successfully');
    } catch (error) {
      alert('Logout failed');
      console.error(error);
    }
  };

  return (
    <div>
      <h2>Logout</h2>
      <button onClick={handleLogout}>Logout</button>
    </div>
  );
}

export default Logout;
